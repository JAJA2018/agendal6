<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imobiliaria', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('corretor_imovel',70);
            $table->string('codigo_imovel',10);
            $table->date('data_cadastro_imovel',10);
            $table->string('tipo_imovel',20);
            $table->string('preço_imovel',20);
            $table->string('locação_imovel',20);
            $table->string('status_imovel',20);
            $table->string('status_ocupação_imovel',20);
            $table->string('custo_condominio_imovel',20);
            $table->string('nome_imovel',70); 
            $table->string('endereco_imovel',70);
            $table->string('cep_imovel',15);
            $table->date('ano_costrução',10);
            $table->string('area_total',10);
            $table->string('area_construida',10);
            $table->string('iptu_imovel',10);
            $table->string('parcela-iptu',10);
            $table->string('Bloco_imovel',15);
            $table->string('Andar_imovel',10);
            $table->string('quanti_comodo',10);
            $table->string('quanti_quarto',10);
            $table->string('quanti_cozinha',20);
            $table->string('quanti_sala',10);
            $table->string('quanti_banheiro',10);
            $table->string('quanti_vagas_garagem',10);
            


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
