<?php

namespace App\Http\Controllers;

use App\imobiliaria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImobiliariaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\imobiliaria  $imobiliaria
     * @return \Illuminate\Http\Response
     */
    public function show(imobiliaria $imobiliaria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\imobiliaria  $imobiliaria
     * @return \Illuminate\Http\Response
     */
    public function edit(imobiliaria $imobiliaria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\imobiliaria  $imobiliaria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, imobiliaria $imobiliaria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\imobiliaria  $imobiliaria
     * @return \Illuminate\Http\Response
     */
    public function destroy(imobiliaria $imobiliaria)
    {
        //
    }
}
